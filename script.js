const gameContainer = document.getElementById("game");

const colors = [
  "red",
  "blue",
  "green",
  "orange",
  "purple",
  "red",
  "blue",
  "green",
  "orange",
  "purple"
];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);
    // console.log(index);
    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledColors = shuffle(colors);
console.log(shuffledColors);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
    let index =0;
  for (let color of colorArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(color);
    newDiv.setAttribute("title",index++);

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

let clickInputs = [];

// TODO: Implement this function!
function handleCardClick(event) {
  // you can use event.target to see which element was clicked
//   console.log("you clicked",event.target);
  event.target.style.backgroundColor = event.target.classList;
  let isPush = 0;
  let clickCount = 0;

  if(clickInputs[clickInputs.length] === 0) {
    isPush = clickInputs.push(event.target);
  } else {
  clickInputs.forEach(element => {
    // console.log(element.getAttribute("class")+ element.getAttribute("title"));
    if((element.getAttribute("class") + element.getAttribute("title")) === (event.target.getAttribute("class") + event.target.getAttribute("title"))) {
        clickCount++;
    }
  });
}

if (clickCount === 0){
  isPush = clickInputs.push(event.target);
}

console.log(clickInputs);
    if(isPush){
    if(clickInputs.length>1 && clickInputs.length%2 === 0 && (clickInputs[clickInputs.length-1].getAttribute("style")  !== clickInputs[clickInputs.length-2].getAttribute("style"))) {
        setTimeout(function() {
            clickInputs[clickInputs.length-1].style.backgroundColor = "white";
            clickInputs[clickInputs.length-2].style.backgroundColor = "white";
            clickInputs.pop(clickInputs[clickInputs.length-1]);
            clickInputs.pop(clickInputs[clickInputs.length-2]);
        },500);  
    }
  }
}

// when the DOM loads
createDivsForColors(shuffledColors);

